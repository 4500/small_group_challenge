# user types a string x and pushes ENTER
# then the next line on the screen the program displays
# "Hello, X. Press ENTER to finish this program."
# when user pushes enter after your hello message
# the program should finish normally

# variable to store users name
x = input();

# printing hello message
print("hello,", x, "Press ENTER to finish this program")

# delay exit until user types enter
y = input();
